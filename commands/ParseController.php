<?php

namespace app\commands;

use GEO\City\Repository\YiiCityRepository;
use GEO\Provider\APIGEOProvider;
use GEO\Region\Repository\YiiRegionRepository;
use GuzzleHttp\Client;
use Yii;
use yii\console\Controller;

class ParseController extends Controller
{
    public function actionIndex()
    {
        $cityRepository = new YiiCityRepository(Yii::$app->db);

        $regionRepository = new YiiRegionRepository(Yii::$app->db);

        $client = new Client();

        $geoProvider = new APIGEOProvider($client);

        $regionRepository->addCollection(
            $geoProvider->provideRegions()
        );

        $cityRepository->addCollection(
            $geoProvider->provideCities()
        );
    }
}
