<?php

use yii\db\Migration;

/**
 * m170616_145902_create_region_table
 */
class m170616_145902_create_region_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('region');
    }

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('region', [
            'identity' => $this->string(),
            'name' => $this->string()->unique(),
        ]);

        $this->addPrimaryKey('regionIdentity', 'region', ['identity']);
    }
}
