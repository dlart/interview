<?php

use yii\db\Migration;

/**
 * m170616_145919_create_city_table
 */
class m170616_145919_create_city_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('region', 'city');

        $this->dropTable('city');
    }

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('city', [
            'identity' => $this->string(),
            'regionIdentity' => $this->string(),
            'name' => $this->string(),
        ]);

        $this->addForeignKey('cityRegion', 'city', 'regionIdentity', 'region', 'identity');
        $this->addPrimaryKey('cityIdentity', 'city', ['identity']);
    }
}
