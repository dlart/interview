<?php

namespace Core\Collection;

use ArrayIterator;
use IteratorAggregate;

/**
 * AbstractCollection
 */
abstract class AbstractCollection implements IteratorAggregate
{
    /**
     * @var array
     */
    private $items = [];

    /**
     * @param array $items
     */
    public function __construct(array $items = [])
    {
        foreach ($items as $item) {
            $this->assertThatItemIsValid($item);
        }

        $this->items = $items;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }

    /**
     * @param mixed $item
     */
    protected function assertThatItemIsValid($item): void
    {
    }
}
