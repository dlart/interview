<?php

namespace Core;

use Core\Identity\IdentityInterface as Identity;

/**
 * HasIdentityInterface
 */
interface HasIdentityInterface
{
    /**
     * @return Identity
     */
    public function getIdentity(): Identity;
}
