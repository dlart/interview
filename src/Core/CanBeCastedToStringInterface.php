<?php

namespace Core;

/**
 * CanBeCastedToStringInterface
 */
interface CanBeCastedToStringInterface
{
    /**
     * @return string
     */
    public function __toString(): string;
}
