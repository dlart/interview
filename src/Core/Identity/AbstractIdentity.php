<?php

namespace Core\Identity;

use Core\Identity\IdentityInterface as Identity;
use Core\ValueObject\AbstractValueObject;

/**
 * AbstractIdentity
 */
abstract class AbstractIdentity extends AbstractValueObject implements Identity
{
}
