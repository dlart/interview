<?php

namespace Core\Identity\String\UUID;

use Assert\Assertion;
use Core\Identity\String\AbstractStringIdentity as StringIdentity;

/**
 * AbstractUUIDIdentity
 */
abstract class AbstractUUIDIdentity extends StringIdentity
{
    /**
     * @param string $identity
     */
    protected function assertThatIdentityIsValid(string $identity): void
    {
        Assertion::uuid($identity);
    }

    /**
     * @param string $identity
     *
     * @return string
     */
    protected function normalizeIdentityBeforeCompare(string $identity): string
    {
        return strtolower(
            parent::normalizeIdentityBeforeCompare($identity)
        );
    }
}
