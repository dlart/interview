<?php

namespace Core\Identity\String;

use Core\Identity\AbstractIdentity;
use Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * AbstractStringIdentity
 */
abstract class AbstractStringIdentity extends AbstractIdentity
{
    /**
     * @var string
     */
    private $identity;

    /**
     * @param string $identity
     */
    public function __construct(string $identity)
    {
        $this->assertThatIdentityIsValid($identity);

        $this->identity = $identity;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->identity;
    }

    /**
     * @param ValueObject $valueObject
     *
     * @return bool
     */
    public function isEqualTo(ValueObject $valueObject): bool
    {
        return
            parent::isEqualTo($valueObject)
            && $this->normalizeIdentityBeforeCompare((string) $this)
            === $this->normalizeIdentityBeforeCompare((string) $valueObject)
        ;
    }

    /**
     * @param string $identity
     */
    protected function assertThatIdentityIsValid(string $identity): void
    {
    }

    /**
     * @param string $identity
     *
     * @return string
     */
    protected function normalizeIdentityBeforeCompare(string $identity): string
    {
        return $identity;
    }
}
