<?php

namespace Core\Identity;

use Core\CanBeCastedToStringInterface as CanBeCastedToString;
use Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * IdentityInterface
 */
interface IdentityInterface extends CanBeCastedToString, ValueObject
{
}
