<?php

namespace Core\ValueObject;

use Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * AbstractValueObject
 */
abstract class AbstractValueObject implements ValueObject
{
    /**
     * @param ValueObject $valueObject
     *
     * @return bool
     */
    public function isEqualTo(ValueObject $valueObject): bool
    {
        return $valueObject instanceof static;
    }
}
