<?php

namespace Core\Aggregate\Root;

use Core\HasIdentityInterface as HasIdentity;

/**
 * AggregateRootInterface
 */
interface AggregateRootInterface extends HasIdentity
{
}
