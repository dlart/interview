<?php

namespace GEO\Region;

use Assert\Assertion;
use Core\Collection\AbstractCollection as Collection;
use GEO\Region;

/**
 * RegionCollection
 */
class RegionCollection extends Collection
{
    /**
     * @inheritdoc
     */
    protected function assertThatItemIsValid($item): void
    {
        Assertion::isInstanceOf($item, Region::class);
    }
}
