<?php

namespace GEO\Region\Repository;

use GEO\Region\RegionCollection;

/**
 * RegionRepositoryInterface
 */
interface RegionRepositoryInterface
{
    /**
     * @param RegionCollection $regionCollection
     */
    public function addCollection(RegionCollection $regionCollection): void;
}
