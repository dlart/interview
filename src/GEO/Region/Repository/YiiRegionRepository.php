<?php

namespace GEO\Region\Repository;

use GEO\Region\Repository\RegionRepositoryInterface as RegionRepository;
use GEO\Region;
use GEO\Region\RegionCollection;
use yii\db\Connection;

/**
 * YiiRegionRepository
 */
class YiiRegionRepository implements RegionRepository
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param RegionCollection $regionCollection
     */
    public function addCollection(RegionCollection $regionCollection): void
    {
        $sql = 'INSERT INTO region (identity, name) VALUES (%s)';

        $values = [];

        foreach ($regionCollection as $region) {
            /** @var Region $region $ */
            $values[] = implode(', ', [
                sprintf('\'%s\'', $region->getIdentity()),
                sprintf('\'%s\'', mb_convert_encoding($region->getName(), "UTF8")),
            ]);
        }

        $sql = sprintf($sql, implode('), (', $values));

        $this
            ->connection
            ->createCommand($sql)
            ->execute()
        ;
    }
}
