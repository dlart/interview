<?php

namespace GEO\Region;

use Core\Identity\String\UUID\AbstractUUIDIdentity as UUIDIdentity;

/**
 * RegionIdentity
 */
class RegionIdentity extends UUIDIdentity
{
}
