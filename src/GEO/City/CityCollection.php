<?php

namespace GEO\City;

use Assert\Assertion;
use Core\Collection\AbstractCollection as Collection;
use GEO\City;

/**
 * CityCollection
 */
class CityCollection extends Collection
{
    /**
     * @inheritdoc
     */
    protected function assertThatItemIsValid($item): void
    {
        Assertion::isInstanceOf($item, City::class);
    }
}
