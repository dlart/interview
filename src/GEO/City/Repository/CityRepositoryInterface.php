<?php

namespace GEO\City\Repository;

use GEO\City\CityCollection;

/**
 * CityRepositoryInterface
 */
interface CityRepositoryInterface
{
    /**
     * @param CityCollection $cityCollection
     */
    public function addCollection(CityCollection $cityCollection): void;
}
