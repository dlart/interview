<?php

namespace GEO\City\Repository;

use GEO\City;
use GEO\City\CityCollection;
use GEO\City\Repository\CityRepositoryInterface as CityRepository;
use yii\db\Connection;

/**
 * YiiCityRepository
 */
class YiiCityRepository implements CityRepository
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param CityCollection $cityCollection
     */
    public function addCollection(CityCollection $cityCollection): void
    {
        $sql = 'INSERT INTO city (identity, regionIdentity, name) VALUES (%s)';

        $values = [];

        foreach ($cityCollection as $city) {
            /** @var City $city */
            $values[] = implode(', ', [
                sprintf('\'%s\'', $city->getIdentity()),
                sprintf('\'%s\'', $city->getRegionIdentity()),
                sprintf('\'%s\'', $city->getName())
            ]);
        }

        $sql = sprintf($sql, implode('), (', $values));

        $this
            ->connection
            ->createCommand($sql)
            ->execute()
        ;
    }
}
