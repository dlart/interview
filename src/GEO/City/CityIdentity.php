<?php

namespace GEO\City;

use Core\Identity\String\UUID\AbstractUUIDIdentity as UUIDIdentity;

/**
 * CityIdentity
 */
class CityIdentity extends UUIDIdentity
{
}
