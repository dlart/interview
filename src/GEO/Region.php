<?php

namespace GEO;

use Assert\Assertion;
use Core\Aggregate\Root\AggregateRootInterface as AggregateRoot;
use Core\Identity\IdentityInterface as Identity;
use GEO\Region\RegionIdentity;

/**
 * Region
 */
class Region implements AggregateRoot
{
    /**
     * @var RegionIdentity
     */
    private $identity;

    /**
     * @var string
     */
    private $name;

    /**
     * @param RegionIdentity $identity
     * @param string         $name
     *
     * @return Region
     */
    public static function plugIn(
        RegionIdentity $identity,
        string $name
    ): Region {
        return new self(
            $identity,
            $name
        );
    }

    /**
     * @param RegionIdentity $identity
     * @param string         $name
     */
    private function __construct(
        RegionIdentity $identity,
        string $name
    ) {
        $this->identity = $identity;

        Assertion::notEmpty($name);

        $this->name = $name;
    }

    /**
     * @return Identity
     */
    public function getIdentity(): Identity
    {
        return $this->identity;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
