<?php

namespace GEO;

use Assert\Assertion;
use Core\Aggregate\Root\AggregateRootInterface as AggregateRoot;
use Core\Identity\IdentityInterface as Identity;
use GEO\City\CityIdentity;
use GEO\Region\RegionIdentity;

/**
 * City
 */
class City implements AggregateRoot
{
    /**
     * @var CityIdentity
     */
    private $identity;

    /**
     * @var string
     */
    private $name;

    /**
     * @var RegionIdentity
     */
    private $regionIdentity;

    /**
     * @param CityIdentity   $identity
     * @param RegionIdentity $regionIdentity
     * @param string         $name
     *
     * @return City
     */
    public static function plugIn(
        CityIdentity $identity,
        RegionIdentity $regionIdentity,
        string $name
    ): City {
        return new self(
            $identity,
            $regionIdentity,
            $name
        );
    }

    /**
     * @param CityIdentity   $identity
     * @param RegionIdentity $regionIdentity
     * @param string         $name
     */
    private function __construct(
        CityIdentity $identity,
        RegionIdentity $regionIdentity,
        string $name
    ) {
        $this->identity = $identity;
        $this->regionIdentity = $regionIdentity;

        Assertion::notEmpty($name);

        $this->name = $name;
    }

    /**
     * @return Identity
     */
    public function getIdentity(): Identity
    {
        return $this->identity;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return RegionIdentity
     */
    public function getRegionIdentity(): RegionIdentity
    {
        return $this->regionIdentity;
    }
}
