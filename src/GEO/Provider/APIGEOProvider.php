<?php

namespace GEO\Provider;

use GEO\City;
use GEO\City\CityCollection;
use GEO\City\CityIdentity;
use GEO\Provider\API\Exception\APIException;
use GEO\Provider\GEOProviderInterface as GEOProvider;
use GEO\Region;
use GEO\Region\RegionCollection;
use GEO\Region\RegionIdentity;
use GuzzleHttp\Client;
use Ramsey\Uuid\Uuid;

/**
 * APIGEOProvider
 */
class APIGEOProvider implements GEOProvider
{
    const API_ENDPOINT = 'https://pecom.ru/ru/calc/towns.php';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var array
     */
    private $cache;

    /**
     * @var array
     */
    private $cityList;

    /**
     * @var array
     */
    private $regionList;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return CityCollection
     */
    public function provideCities(): CityCollection
    {
        if (!$this->cityList) {
            $data = $this->getData();

            $this->provideRegions();

            $collection = [];

            foreach ($data as $regionName => $cityList) {
                foreach ($cityList as $cityName) {
                    $cityIdentity = new CityIdentity(Uuid::uuid4());

                    if (!isset($this->regionList[$regionName])) {
                        $regionIdentity = new RegionIdentity(Uuid::uuid4());
                    } else {
                        $regionIdentity = $this->regionList[$regionName]->getIdentity();
                    }

                    $collection[(string) $cityIdentity] = City::plugIn(
                        $cityIdentity,
                        $regionIdentity,
                        $this->removeEmoji($cityName)
                    );

                    $this->cityList = $collection;
                }
            }
        }

        return new CityCollection($this->cityList);
    }

    /**
     * @return RegionCollection
     */
    public function provideRegions(): RegionCollection
    {
        if (!$this->regionList) {
            $data = $this->getData();

            $regionList = [];

            foreach (array_keys($data) as $regionName) {
                $regionIdentity = new RegionIdentity(Uuid::uuid4());

                $regionList[$regionName] = Region::plugIn(
                    $regionIdentity,
                    $this->removeEmoji($regionName)
                );
            }

            $this->regionList = $regionList;
        }

        return new RegionCollection($this->regionList);
    }

    /**
     * @return array
     */
    private function getData(): array
    {
        if (!$this->cache) {
            $response = $this->client->get(self::API_ENDPOINT);

            if (200 !== $response->getStatusCode()) {
                throw new APIException(
                    'External API return non 200 status code.'
                );
            }

            $body = $response->getBody();

            $body = json_decode($body, true);

            $this->cache = $body;
        }

        return $this->cache;
    }

    /**
     * @param string $text
     *
     * @return string
     */
    private function removeEmoji(string $text): string
    {
        $clean_text = "";

        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace($regexEmoticons, '', $text);
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clean_text = preg_replace($regexSymbols, '', $clean_text);
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clean_text = preg_replace($regexTransport, '', $clean_text);
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        $clean_text = preg_replace($regexMisc, '', $clean_text);
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);

        return $clean_text;
    }
}
