<?php

namespace GEO\Provider;

use GEO\City\CityCollection;
use GEO\Region\RegionCollection;

/**
 * GEOProviderInterface
 */
interface GEOProviderInterface
{
    /**
     * @return CityCollection
     */
    public function provideCities(): CityCollection;

    /**
     * @return RegionCollection
     */
    public function provideRegions(): RegionCollection;
}
