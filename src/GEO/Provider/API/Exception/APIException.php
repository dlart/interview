<?php

namespace GEO\Provider\API\Exception;

use RuntimeException;

/**
 * APIException
 */
class APIException extends RuntimeException
{
}
